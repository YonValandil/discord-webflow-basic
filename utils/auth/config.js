export default {
  baseurl: 'https://discord.com',
  redirect_uri: '',
  data: {
    grant_type: 'authorization_code',
    client_id: '',
    client_secret: '',
    port: 3000
  }
}

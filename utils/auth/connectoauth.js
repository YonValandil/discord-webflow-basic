import axios from 'axios'
import qs from 'qs'
import config from './config.js'
import oauth from 'axios-oauth-client'

let SINGLETON = 0

const client = axios.create({
  baseURL: config.baseurl
})

// Initialize OauthData here allows not to overwrite the refresh_token value
let oauthdata = {
  url: 'https://discord.com/api/oauth2/token',
  grant_type: config.data.grant_type,
  client_id: null,
  client_secret: null,
  redirect_uri: null,
  code: null,
  refresh_token: null,
  scope: 'identify'
}

const getToken = async (client, data) => {
  try {
    const config = {
      headers: {
        'content-type': 'application/x-www-form-urlencoded'
      }
    }

    const getOwnerCredentials = oauth.client(axios.create(), {
      url: data.url,
      client_id: data.client_id,
      client_secret: data.client_secret,
      code: data.code,
      grant_type: data.grant_type,
      redirect_uri: data.redirect_uri,
      scope: data.scope
    }, config)
    const auth = await getOwnerCredentials()

    client.defaults.headers.common.Authorization = `${auth.token_type} ${auth.access_token}`
    oauthdata.refresh_token = auth.refresh_token

    console.log('token => ', auth)

    return auth
  } catch (error) {
    console.error(error.response.data)
  }
}

const getRefresh = async (client, oauthdata) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }

    const reqdata = qs.stringify({
      client_id: oauthdata.client_id,
      client_secret: oauthdata.client_secret,
      refresh_token: oauthdata.refresh_token,
      grant_type: 'refresh_token'
    })

    const token = await client.post('/api/oauth2/token', reqdata, config)
    client.defaults.headers.common.Authorization = `${token.data.token_type} ${token.data.access_token}`
    oauthdata.refresh_token = token.data.refresh_token

    console.log('refresh token => ', token.data)

    return token.data
  } catch (error) {
    console.error(error.response.data)
  }
}

const interceptor = async (client, oauthdata) => {
  client.interceptors.response.use(
    success => success,
    async error => {
      const originalRequest = error.config
      if (
        error.response.status === 401 &&
          originalRequest.url.includes('/api/oauth2/token')
      ) {
        return Promise.reject(error)
      } else if (error.response.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true
        const token = await getRefresh(client, oauthdata)
        originalRequest.headers.Authorization = `${token.token_type} ${token.access_token}`
        return client(originalRequest)
      }
      return Promise.reject(error)
    }
  )
}

export const connectoauth = async (
  req,
  client_id = config.data.client_id,
  client_secret = config.data.client_secret,
  redirect_uri = config.redirect_uri
) => {
  oauthdata = {
    ...oauthdata,
    client_id: client_id,
    client_secret: client_secret,
    redirect_uri: redirect_uri,
    code: req.rawHeaders[17].match(/\?code=(.+)&state/)[1]
  }

  if (SINGLETON === 0) {
    await getToken(client, oauthdata)
    SINGLETON++
  }

  await interceptor(client, oauthdata)

  return client
}

/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function() {
var exports = {};
exports.id = "pages/dashboard";
exports.ids = ["pages/dashboard"];
exports.modules = {

/***/ "./pages/dashboard/index.js":
/*!**********************************!*\
  !*** ./pages/dashboard/index.js ***!
  \**********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ \"axios\");\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var atob__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! atob */ \"atob\");\n/* harmony import */ var atob__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(atob__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/styles */ \"./node_modules/@material-ui/core/esm/styles/index.js\");\n/* harmony import */ var _material_ui_core_Container__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/core/Container */ \"./node_modules/@material-ui/core/esm/Container/index.js\");\n/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core/Box */ \"./node_modules/@material-ui/core/esm/Box/index.js\");\n/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core/Button */ \"./node_modules/@material-ui/core/esm/Button/index.js\");\n/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core */ \"@material-ui/core\");\n/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__);\n\n\nvar _jsxFileName = \"/Users/yonvalandil/Documents/dev/discordOauth/pages/dashboard/index.js\";\n\n\n\n\n\n\n\n\n\nconst useStyles = (0,_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_6__.makeStyles)(() => ({\n  container: {\n    margin: '10px',\n    alignItems: 'center',\n    justifyContent: 'center'\n  },\n  title: {\n    textAlign: 'center',\n    marginBottom: '20px'\n  },\n  button: {\n    margin: 'auto',\n    width: 120\n  },\n  loader: {\n    display: 'block',\n    marginLeft: '40px',\n    marginTop: '20px'\n  }\n}));\n\nconst Dashboard = () => {\n  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();\n  const classes = useStyles();\n  const {\n    0: loading,\n    1: setLoading\n  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);\n  const {\n    0: data,\n    1: setData\n  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);\n  const {\n    0: clickjacked,\n    1: setclickjacked\n  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);\n  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {\n    if (router.query.state === undefined) {\n      return;\n    }\n\n    if (window.localStorage.getItem('oauth-state') !== decodeURIComponent(atob__WEBPACK_IMPORTED_MODULE_4___default()(router.query.state))) {\n      setData(false);\n      setclickjacked(true);\n      return console.log('You may have been click-jacked!');\n    }\n  });\n\n  const handleUsers = async e => {\n    e.preventDefault();\n    setLoading(true);\n\n    if (clickjacked) {\n      setLoading(false);\n      return;\n    }\n\n    const res = await axios__WEBPACK_IMPORTED_MODULE_3___default().get('../api/getUsers').catch(err => console.error(err));\n\n    if (res && res.status === 200) {\n      setData(res.data);\n    }\n\n    setLoading(false);\n  };\n\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {\n    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_material_ui_core_Container__WEBPACK_IMPORTED_MODULE_7__.default, {\n      maxWidth: \"sm\",\n      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_8__.default, {\n        my: 4,\n        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n          className: classes.title,\n          children: \"Dashboard\"\n        }, void 0, false, {\n          fileName: _jsxFileName,\n          lineNumber: 74,\n          columnNumber: 11\n        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h3\", {\n          className: classes.title,\n          children: \"Test user / avatar\"\n        }, void 0, false, {\n          fileName: _jsxFileName,\n          lineNumber: 76,\n          columnNumber: 11\n        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_9__.default, {\n          variant: \"contained\",\n          color: \"secondary\",\n          onClick: handleUsers,\n          children: \"Send request\"\n        }, void 0, false, {\n          fileName: _jsxFileName,\n          lineNumber: 77,\n          columnNumber: 11\n        }, undefined), loading && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__.CircularProgress, {\n          className: classes.loader\n        }, void 0, false, {\n          fileName: _jsxFileName,\n          lineNumber: 81,\n          columnNumber: 23\n        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"p\", {\n          children: [!loading && clickjacked && 'You may have been click-jacked!', !loading && data && Object.values(data).map((value, key) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"span\", {\n            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"strong\", {\n              children: [Object.keys(data).filter((v, k) => {\n                return k === key;\n              }), \":\"]\n            }, void 0, true, {\n              fileName: _jsxFileName,\n              lineNumber: 86,\n              columnNumber: 17\n            }, undefined), ' ', value, /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"br\", {}, void 0, false, {\n              fileName: _jsxFileName,\n              lineNumber: 88,\n              columnNumber: 17\n            }, undefined)]\n          }, key, true, {\n            fileName: _jsxFileName,\n            lineNumber: 85,\n            columnNumber: 15\n          }, undefined))]\n        }, void 0, true, {\n          fileName: _jsxFileName,\n          lineNumber: 82,\n          columnNumber: 11\n        }, undefined)]\n      }, void 0, true, {\n        fileName: _jsxFileName,\n        lineNumber: 73,\n        columnNumber: 9\n      }, undefined)\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 72,\n      columnNumber: 7\n    }, undefined)\n  }, void 0, false);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Dashboard);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly93ZWJmbG93LWRpc2NvcmQvLi9wYWdlcy9kYXNoYm9hcmQvaW5kZXguanM/ZTU0OSJdLCJuYW1lcyI6WyJ1c2VTdHlsZXMiLCJtYWtlU3R5bGVzIiwiY29udGFpbmVyIiwibWFyZ2luIiwiYWxpZ25JdGVtcyIsImp1c3RpZnlDb250ZW50IiwidGl0bGUiLCJ0ZXh0QWxpZ24iLCJtYXJnaW5Cb3R0b20iLCJidXR0b24iLCJ3aWR0aCIsImxvYWRlciIsImRpc3BsYXkiLCJtYXJnaW5MZWZ0IiwibWFyZ2luVG9wIiwiRGFzaGJvYXJkIiwicm91dGVyIiwidXNlUm91dGVyIiwiY2xhc3NlcyIsImxvYWRpbmciLCJzZXRMb2FkaW5nIiwidXNlU3RhdGUiLCJkYXRhIiwic2V0RGF0YSIsImNsaWNramFja2VkIiwic2V0Y2xpY2tqYWNrZWQiLCJ1c2VFZmZlY3QiLCJxdWVyeSIsInN0YXRlIiwidW5kZWZpbmVkIiwid2luZG93IiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsImRlY29kZVVSSUNvbXBvbmVudCIsImF0b2IiLCJjb25zb2xlIiwibG9nIiwiaGFuZGxlVXNlcnMiLCJlIiwicHJldmVudERlZmF1bHQiLCJyZXMiLCJheGlvcyIsImNhdGNoIiwiZXJyIiwiZXJyb3IiLCJzdGF0dXMiLCJPYmplY3QiLCJ2YWx1ZXMiLCJtYXAiLCJ2YWx1ZSIsImtleSIsImtleXMiLCJmaWx0ZXIiLCJ2IiwiayJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSxNQUFNQSxTQUFTLEdBQUdDLG9FQUFVLENBQUMsT0FBTztBQUNsQ0MsV0FBUyxFQUFFO0FBQ1RDLFVBQU0sRUFBRSxNQURDO0FBRVRDLGNBQVUsRUFBRSxRQUZIO0FBR1RDLGtCQUFjLEVBQUU7QUFIUCxHQUR1QjtBQU1sQ0MsT0FBSyxFQUFFO0FBQ0xDLGFBQVMsRUFBRSxRQUROO0FBRUxDLGdCQUFZLEVBQUU7QUFGVCxHQU4yQjtBQVVsQ0MsUUFBTSxFQUFFO0FBQ05OLFVBQU0sRUFBRSxNQURGO0FBRU5PLFNBQUssRUFBRTtBQUZELEdBVjBCO0FBY2xDQyxRQUFNLEVBQUU7QUFDTkMsV0FBTyxFQUFFLE9BREg7QUFFTkMsY0FBVSxFQUFFLE1BRk47QUFHTkMsYUFBUyxFQUFFO0FBSEw7QUFkMEIsQ0FBUCxDQUFELENBQTVCOztBQXFCQSxNQUFNQyxTQUFTLEdBQUcsTUFBTTtBQUN0QixRQUFNQyxNQUFNLEdBQUdDLHNEQUFTLEVBQXhCO0FBQ0EsUUFBTUMsT0FBTyxHQUFHbEIsU0FBUyxFQUF6QjtBQUNBLFFBQU07QUFBQSxPQUFDbUIsT0FBRDtBQUFBLE9BQVVDO0FBQVYsTUFBd0JDLCtDQUFRLENBQUMsS0FBRCxDQUF0QztBQUNBLFFBQU07QUFBQSxPQUFDQyxJQUFEO0FBQUEsT0FBT0M7QUFBUCxNQUFrQkYsK0NBQVEsQ0FBQyxLQUFELENBQWhDO0FBQ0EsUUFBTTtBQUFBLE9BQUNHLFdBQUQ7QUFBQSxPQUFjQztBQUFkLE1BQWdDSiwrQ0FBUSxDQUFDLEtBQUQsQ0FBOUM7QUFFQUssa0RBQVMsQ0FBQyxNQUFNO0FBQ2QsUUFBSVYsTUFBTSxDQUFDVyxLQUFQLENBQWFDLEtBQWIsS0FBdUJDLFNBQTNCLEVBQXNDO0FBQ3BDO0FBQ0Q7O0FBRUQsUUFBSUMsTUFBTSxDQUFDQyxZQUFQLENBQW9CQyxPQUFwQixDQUE0QixhQUE1QixNQUErQ0Msa0JBQWtCLENBQUNDLDJDQUFJLENBQUNsQixNQUFNLENBQUNXLEtBQVAsQ0FBYUMsS0FBZCxDQUFMLENBQXJFLEVBQWlHO0FBQy9GTCxhQUFPLENBQUMsS0FBRCxDQUFQO0FBQ0FFLG9CQUFjLENBQUMsSUFBRCxDQUFkO0FBQ0EsYUFBT1UsT0FBTyxDQUFDQyxHQUFSLENBQVksaUNBQVosQ0FBUDtBQUNEO0FBQ0YsR0FWUSxDQUFUOztBQVlBLFFBQU1DLFdBQVcsR0FBRyxNQUFPQyxDQUFQLElBQWE7QUFDL0JBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBbkIsY0FBVSxDQUFDLElBQUQsQ0FBVjs7QUFFQSxRQUFJSSxXQUFKLEVBQWlCO0FBQ2ZKLGdCQUFVLENBQUMsS0FBRCxDQUFWO0FBQ0E7QUFDRDs7QUFFRCxVQUFNb0IsR0FBRyxHQUFHLE1BQU1DLGdEQUFBLENBQVUsaUJBQVYsRUFDZkMsS0FEZSxDQUNUQyxHQUFHLElBQUlSLE9BQU8sQ0FBQ1MsS0FBUixDQUFjRCxHQUFkLENBREUsQ0FBbEI7O0FBR0EsUUFBSUgsR0FBRyxJQUFJQSxHQUFHLENBQUNLLE1BQUosS0FBZSxHQUExQixFQUErQjtBQUM3QnRCLGFBQU8sQ0FBQ2lCLEdBQUcsQ0FBQ2xCLElBQUwsQ0FBUDtBQUNEOztBQUNERixjQUFVLENBQUMsS0FBRCxDQUFWO0FBQ0QsR0FoQkQ7O0FBa0JBLHNCQUNFO0FBQUEsMkJBQ0UsOERBQUMsZ0VBQUQ7QUFBVyxjQUFRLEVBQUMsSUFBcEI7QUFBQSw2QkFDRSw4REFBQywwREFBRDtBQUFLLFVBQUUsRUFBRSxDQUFUO0FBQUEsZ0NBQ0U7QUFBSSxtQkFBUyxFQUFFRixPQUFPLENBQUNaLEtBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURGLGVBR0U7QUFBSSxtQkFBUyxFQUFFWSxPQUFPLENBQUNaLEtBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUhGLGVBSUUsOERBQUMsNkRBQUQ7QUFBUSxpQkFBTyxFQUFDLFdBQWhCO0FBQTRCLGVBQUssRUFBQyxXQUFsQztBQUE4QyxpQkFBTyxFQUFFK0IsV0FBdkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBSkYsRUFRR2xCLE9BQU8saUJBQUksOERBQUMsK0RBQUQ7QUFBa0IsbUJBQVMsRUFBRUQsT0FBTyxDQUFDUDtBQUFyQztBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQVJkLGVBU0U7QUFBQSxxQkFDRyxDQUFDUSxPQUFELElBQVlLLFdBQVosSUFBMkIsaUNBRDlCLEVBRUcsQ0FBQ0wsT0FBRCxJQUFZRyxJQUFaLElBQW9Cd0IsTUFBTSxDQUFDQyxNQUFQLENBQWN6QixJQUFkLEVBQW9CMEIsR0FBcEIsQ0FBd0IsQ0FBQ0MsS0FBRCxFQUFRQyxHQUFSLGtCQUMzQztBQUFBLG9DQUNFO0FBQUEseUJBQVNKLE1BQU0sQ0FBQ0ssSUFBUCxDQUFZN0IsSUFBWixFQUFrQjhCLE1BQWxCLENBQXlCLENBQUNDLENBQUQsRUFBSUMsQ0FBSixLQUFVO0FBQUUsdUJBQU9BLENBQUMsS0FBS0osR0FBYjtBQUFrQixlQUF2RCxDQUFUO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFERixFQUMrRSxHQUQvRSxFQUVHRCxLQUZILGVBR0U7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFIRjtBQUFBLGFBQVdDLEdBQVg7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFEbUIsQ0FGdkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQVRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixtQkFERjtBQTBCRCxDQS9ERDs7QUFpRUEsK0RBQWVuQyxTQUFmIiwiZmlsZSI6Ii4vcGFnZXMvZGFzaGJvYXJkL2luZGV4LmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCdcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gJ25leHQvcm91dGVyJ1xuaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJ1xuaW1wb3J0IGF0b2IgZnJvbSAnYXRvYidcblxuaW1wb3J0IHsgbWFrZVN0eWxlcyB9IGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlL3N0eWxlcydcbmltcG9ydCBDb250YWluZXIgZnJvbSAnQG1hdGVyaWFsLXVpL2NvcmUvQ29udGFpbmVyJ1xuaW1wb3J0IEJveCBmcm9tICdAbWF0ZXJpYWwtdWkvY29yZS9Cb3gnXG5pbXBvcnQgQnV0dG9uIGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlL0J1dHRvbidcbmltcG9ydCB7IENpcmN1bGFyUHJvZ3Jlc3MgfSBmcm9tICdAbWF0ZXJpYWwtdWkvY29yZSdcblxuY29uc3QgdXNlU3R5bGVzID0gbWFrZVN0eWxlcygoKSA9PiAoe1xuICBjb250YWluZXI6IHtcbiAgICBtYXJnaW46ICcxMHB4JyxcbiAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICBqdXN0aWZ5Q29udGVudDogJ2NlbnRlcidcbiAgfSxcbiAgdGl0bGU6IHtcbiAgICB0ZXh0QWxpZ246ICdjZW50ZXInLFxuICAgIG1hcmdpbkJvdHRvbTogJzIwcHgnXG4gIH0sXG4gIGJ1dHRvbjoge1xuICAgIG1hcmdpbjogJ2F1dG8nLFxuICAgIHdpZHRoOiAxMjBcbiAgfSxcbiAgbG9hZGVyOiB7XG4gICAgZGlzcGxheTogJ2Jsb2NrJyxcbiAgICBtYXJnaW5MZWZ0OiAnNDBweCcsXG4gICAgbWFyZ2luVG9wOiAnMjBweCdcbiAgfVxufSkpXG5cbmNvbnN0IERhc2hib2FyZCA9ICgpID0+IHtcbiAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKClcbiAgY29uc3QgY2xhc3NlcyA9IHVzZVN0eWxlcygpXG4gIGNvbnN0IFtsb2FkaW5nLCBzZXRMb2FkaW5nXSA9IHVzZVN0YXRlKGZhbHNlKVxuICBjb25zdCBbZGF0YSwgc2V0RGF0YV0gPSB1c2VTdGF0ZShmYWxzZSlcbiAgY29uc3QgW2NsaWNramFja2VkLCBzZXRjbGlja2phY2tlZF0gPSB1c2VTdGF0ZShmYWxzZSlcblxuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGlmIChyb3V0ZXIucXVlcnkuc3RhdGUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgaWYgKHdpbmRvdy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnb2F1dGgtc3RhdGUnKSAhPT0gZGVjb2RlVVJJQ29tcG9uZW50KGF0b2Iocm91dGVyLnF1ZXJ5LnN0YXRlKSkpIHtcbiAgICAgIHNldERhdGEoZmFsc2UpXG4gICAgICBzZXRjbGlja2phY2tlZCh0cnVlKVxuICAgICAgcmV0dXJuIGNvbnNvbGUubG9nKCdZb3UgbWF5IGhhdmUgYmVlbiBjbGljay1qYWNrZWQhJylcbiAgICB9XG4gIH0pXG5cbiAgY29uc3QgaGFuZGxlVXNlcnMgPSBhc3luYyAoZSkgPT4ge1xuICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgIHNldExvYWRpbmcodHJ1ZSlcblxuICAgIGlmIChjbGlja2phY2tlZCkge1xuICAgICAgc2V0TG9hZGluZyhmYWxzZSlcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGF4aW9zLmdldCgnLi4vYXBpL2dldFVzZXJzJylcbiAgICAgIC5jYXRjaChlcnIgPT4gY29uc29sZS5lcnJvcihlcnIpKVxuXG4gICAgaWYgKHJlcyAmJiByZXMuc3RhdHVzID09PSAyMDApIHtcbiAgICAgIHNldERhdGEocmVzLmRhdGEpXG4gICAgfVxuICAgIHNldExvYWRpbmcoZmFsc2UpXG4gIH1cblxuICByZXR1cm4gKFxuICAgIDw+XG4gICAgICA8Q29udGFpbmVyIG1heFdpZHRoPSdzbSc+XG4gICAgICAgIDxCb3ggbXk9ezR9PlxuICAgICAgICAgIDxoMSBjbGFzc05hbWU9e2NsYXNzZXMudGl0bGV9PkRhc2hib2FyZDwvaDE+XG5cbiAgICAgICAgICA8aDMgY2xhc3NOYW1lPXtjbGFzc2VzLnRpdGxlfT5UZXN0IHVzZXIgLyBhdmF0YXI8L2gzPlxuICAgICAgICAgIDxCdXR0b24gdmFyaWFudD0nY29udGFpbmVkJyBjb2xvcj0nc2Vjb25kYXJ5JyBvbkNsaWNrPXtoYW5kbGVVc2Vyc30+XG4gICAgICAgICAgICBTZW5kIHJlcXVlc3RcbiAgICAgICAgICA8L0J1dHRvbj5cblxuICAgICAgICAgIHtsb2FkaW5nICYmIDxDaXJjdWxhclByb2dyZXNzIGNsYXNzTmFtZT17Y2xhc3Nlcy5sb2FkZXJ9IC8+fVxuICAgICAgICAgIDxwPlxuICAgICAgICAgICAgeyFsb2FkaW5nICYmIGNsaWNramFja2VkICYmICdZb3UgbWF5IGhhdmUgYmVlbiBjbGljay1qYWNrZWQhJ31cbiAgICAgICAgICAgIHshbG9hZGluZyAmJiBkYXRhICYmIE9iamVjdC52YWx1ZXMoZGF0YSkubWFwKCh2YWx1ZSwga2V5KSA9PiAoXG4gICAgICAgICAgICAgIDxzcGFuIGtleT17a2V5fT5cbiAgICAgICAgICAgICAgICA8c3Ryb25nPntPYmplY3Qua2V5cyhkYXRhKS5maWx0ZXIoKHYsIGspID0+IHsgcmV0dXJuIGsgPT09IGtleSB9KX06PC9zdHJvbmc+eycgJ31cbiAgICAgICAgICAgICAgICB7dmFsdWV9XG4gICAgICAgICAgICAgICAgPGJyIC8+XG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICkpfVxuICAgICAgICAgIDwvcD5cbiAgICAgICAgPC9Cb3g+XG4gICAgICA8L0NvbnRhaW5lcj5cbiAgICA8Lz5cbiAgKVxufVxuXG5leHBvcnQgZGVmYXVsdCBEYXNoYm9hcmRcbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/dashboard/index.js\n");

/***/ }),

/***/ "@material-ui/core":
/*!************************************!*\
  !*** external "@material-ui/core" ***!
  \************************************/
/***/ (function(module) {

"use strict";
module.exports = require("@material-ui/core");;

/***/ }),

/***/ "@material-ui/styles":
/*!**************************************!*\
  !*** external "@material-ui/styles" ***!
  \**************************************/
/***/ (function(module) {

"use strict";
module.exports = require("@material-ui/styles");;

/***/ }),

/***/ "@material-ui/system":
/*!**************************************!*\
  !*** external "@material-ui/system" ***!
  \**************************************/
/***/ (function(module) {

"use strict";
module.exports = require("@material-ui/system");;

/***/ }),

/***/ "@material-ui/utils":
/*!*************************************!*\
  !*** external "@material-ui/utils" ***!
  \*************************************/
/***/ (function(module) {

"use strict";
module.exports = require("@material-ui/utils");;

/***/ }),

/***/ "atob":
/*!***********************!*\
  !*** external "atob" ***!
  \***********************/
/***/ (function(module) {

"use strict";
module.exports = require("atob");;

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("axios");;

/***/ }),

/***/ "clsx":
/*!***********************!*\
  !*** external "clsx" ***!
  \***********************/
/***/ (function(module) {

"use strict";
module.exports = require("clsx");;

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = require("next/router");;

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/***/ (function(module) {

"use strict";
module.exports = require("prop-types");;

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react-dom":
/*!****************************!*\
  !*** external "react-dom" ***!
  \****************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-dom");;

/***/ }),

/***/ "react-transition-group":
/*!*****************************************!*\
  !*** external "react-transition-group" ***!
  \*****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-transition-group");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = __webpack_require__.X(0, ["vendors-node_modules_material-ui_core_esm_styles_index_js","vendors-node_modules_material-ui_core_esm_Button_index_js","vendors-node_modules_material-ui_core_esm_Box_index_js-node_modules_material-ui_core_esm_Cont-57e50b"], function() { return __webpack_exec__("./pages/dashboard/index.js"); });
module.exports = __webpack_exports__;

})();
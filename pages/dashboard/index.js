import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import axios from 'axios'
import atob from 'atob'

import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import { CircularProgress } from '@material-ui/core'

const useStyles = makeStyles(() => ({
  container: {
    margin: '10px',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    textAlign: 'center',
    marginBottom: '20px'
  },
  button: {
    margin: 'auto',
    width: 120
  },
  loader: {
    display: 'block',
    marginLeft: '40px',
    marginTop: '20px'
  }
}))

const Dashboard = () => {
  const router = useRouter()
  const classes = useStyles()
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState(false)
  const [clickjacked, setclickjacked] = useState(false)

  useEffect(() => {
    if (router.query.state === undefined) {
      return
    }

    if (window.localStorage.getItem('oauth-state') !== decodeURIComponent(atob(router.query.state))) {
      setData(false)
      setclickjacked(true)
      return console.log('You may have been click-jacked!')
    }
  })

  const handleUsers = async (e) => {
    e.preventDefault()
    setLoading(true)

    if (clickjacked) {
      setLoading(false)
      return
    }

    const res = await axios.get('../api/getUsers')
      .catch(err => console.error(err))

    if (res && res.status === 200) {
      setData(res.data)
    }
    setLoading(false)
  }

  return (
    <>
      <Container maxWidth='sm'>
        <Box my={4}>
          <h1 className={classes.title}>Dashboard</h1>

          <h3 className={classes.title}>Test user / avatar</h3>
          <Button variant='contained' color='secondary' onClick={handleUsers}>
            Send request
          </Button>

          {loading && <CircularProgress className={classes.loader} />}
          <p>
            {!loading && clickjacked && 'You may have been click-jacked!'}
            {!loading && data && Object.values(data).map((value, key) => (
              <span key={key}>
                <strong>{Object.keys(data).filter((v, k) => { return k === key })}:</strong>{' '}
                {value}
                <br />
              </span>
            ))}
          </p>
        </Box>
      </Container>
    </>
  )
}

export default Dashboard

import config from '../../utils/config.js'
import { connectoauth } from '../../utils/auth/connectoauth.js'

export default async (req, res) => {
  const client = await connectoauth(req, config.data.client_id, config.data.client_secret, config.redirect_uri)

  const user = await client.get('/api/users/@me').catch(err => console.error(err.response.data))

  res.status(200).json(user.data)
}
